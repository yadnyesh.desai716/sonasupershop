insert into customer(id,name,mobile,address)
values(101, 'Joey', 9090090900, 'New York');

insert into customer(id,name,mobile,address)
values(102, 'Chandler', 8090080900, 'New Jersey');

insert into customer(id,name,mobile,address)
values(103, 'Ross', 7090070900, 'New York');

insert into order_details(id,cust_id,order_date,delivered)
values(20001,101,current_date(), false);

insert into order_details(id,cust_id,order_date,delivered)
values(20002,101,current_date(), true);

insert into order_details(id,cust_id,order_date,delivered)
values(20003,102,current_date(), false);

insert into order_details(id,cust_id,order_date,delivered)
values(20004,102,current_date(), false);

insert into order_details(id,cust_id,order_date,delivered)
values(20005,102,current_date(), true);

insert into order_details(id,cust_id,order_date,delivered)
values(20006,103,current_date(), true);

insert into order_details(id,cust_id,order_date,delivered)
values(20007,103,current_date(), true);

--insert into order_details_products(order_details_id,products_id)
--values(20001,500);

insert into product(id,name,price)
values(500,'Cashew', 50.79);

insert into product(id,name,price)
values(550,'Almonds', 70.77);

insert into product(id,name,price)
values(600,'Nuts', 30.57);

insert into product(id,name,price)
values(650,'Peanuts', 55);

insert into product(id,name,price)
values(700,'Chips', 10.32);

insert into product(id,name,price)
values(750,'Cookies', 50);

insert into product(id,name,price)
values(800,'Oil', 325.53);

insert into product(id,name,price)
values(850,'Coconut Oil', 200);

insert into product(id,name,price)
values(900,'Biscuits', 20.13);

insert into product(id,name,price)
values(950,'Rusk', 70);