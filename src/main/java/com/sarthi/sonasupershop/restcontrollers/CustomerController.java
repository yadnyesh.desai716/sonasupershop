package com.sarthi.sonasupershop.restcontrollers;

import com.sarthi.sonasupershop.entities.Customer;
import com.sarthi.sonasupershop.entities.Order;
import com.sarthi.sonasupershop.entities.Product;
import com.sarthi.sonasupershop.repositories.CustomerRepository;
import com.sarthi.sonasupershop.repositories.OrderRepository;
import com.sarthi.sonasupershop.repositories.ProductRepository;
import jakarta.validation.Valid;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class CustomerController {

    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    public CustomerController(CustomerRepository customerRepository, OrderRepository orderRepository, ProductRepository productRepository) {
        this.customerRepository = customerRepository;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping(path = "/list-customers", produces = "application/json")
    public CollectionModel<List<Customer>> retrieveAllCustomers() {
        if (customerRepository.findAll().isEmpty())
            throw new RuntimeException("No customers found.");

        CollectionModel<List<Customer>> collectionModel = CollectionModel.of(Collections.singleton(customerRepository.findAll()));

        List<WebMvcLinkBuilder> webMvcLinkBuilders = new ArrayList<>();
        for (Customer customer : customerRepository.findAll()) {
            webMvcLinkBuilders.add(linkTo(methodOn(this.getClass()).retrieveAllOrdersOfOneCustomer(customer.getId())));
        }

        for (WebMvcLinkBuilder linkAllOrders : webMvcLinkBuilders) {
            collectionModel.add(linkAllOrders.withRel("all_orders"));
        }

        return collectionModel;
    }

    @GetMapping("/customers/{id}")
    public EntityModel<Customer> retrieveOneCustomer(@PathVariable int id) {
        if (!customerRepository.existsById(id))
            throw new RuntimeException("Customer #" + id + " does not exists.");

        EntityModel<Customer> entityModel =
                EntityModel.of(Objects.requireNonNull(customerRepository.findById(id).orElse(null)));

        WebMvcLinkBuilder linkAllOrders = linkTo(methodOn(this.getClass()).retrieveAllOrdersOfOneCustomer(id));

        entityModel.add(linkAllOrders.withRel("all_orders"));

        return entityModel;
    }

    @GetMapping("/customers/{id}/orders")
    public List<Order> retrieveAllOrdersOfOneCustomer(@PathVariable int id) {
        retrieveOneCustomer(id);
        List<Order> orders = orderRepository.findAllOrdersByCustomerId(id);
        if (orders == null || orders.isEmpty())
            throw new RuntimeException("No orders found for Customer #" + id);

        return orders;
    }

    @GetMapping("/customers/{cust_id}/orders/{order_id}")
    public Order retrieveOneOrderOfOneCustomer(@PathVariable int cust_id, @PathVariable int order_id) {
        retrieveAllOrdersOfOneCustomer(cust_id);
        Order order = orderRepository.findOrderByCustomerIdAndId(cust_id, order_id);
        if (order == null) //   Check if orderRepository.existsByCustomerIdAndId(cust_id, order_id) method can be used
            throw new RuntimeException("Order #" + order_id + " does not exists.");

        return order;
    }

    @PostMapping("/customers")
    public ResponseEntity<Customer> addCustomer(@Valid @RequestBody Customer customer) {
        if (customerRepository.existsByNameAndMobile(customer.getName(), customer.getMobile()))
            throw new RuntimeException("Cannot create new Customer. " + customer.getName() + " with mobile number " + customer.getMobile() +
                    " already exists. Please provide a different name or different mobile number.");
        Customer savedCustomer = customerRepository.save(customer);

        URI uriLocation = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedCustomer.getId())
                .toUri();

        return ResponseEntity.created(uriLocation).build();
    }

    @DeleteMapping("/customers/{id}")
    public void removeCustomer(@PathVariable int id) {
        if (!customerRepository.existsById(id))
            throw new RuntimeException("Cannot Delete customer because Customer #" + id + " does not exists.");

        customerRepository.deleteById(id);
    }

    @PostMapping("/customers/{id}/orders")
    public ResponseEntity<Object> createOrderForCustomer(@PathVariable int id, @Valid @RequestBody Order order) {
        retrieveOneCustomer(id);
        if (order.getProducts() == null || order.getProducts().isEmpty())
            throw new RuntimeException("Products cannot be empty. Please add products in this order.");

        order.setCustomer(customerRepository.findById(id).orElse(null));

        /*Check if the customer's products exists in the Product Table / Repository
        If yes then:
            Fetch those products from Product Table & store it in a temp List,
            Remove the entries from customer's product list
            Add all temp list products in the customer's list
        Finally:
            Save the product in the Table/Repository,
            Save the order.*/

        List<Product> savedProducts = new ArrayList<>();
        for (Product product : order.getProducts()) {
            if (isProductExists(product)) {
                savedProducts.add(productRepository.findByNameAndPrice(product.getName(), product.getPrice()));
            }
        }
        order.getProducts().removeIf(this::isProductExists);
        order.getProducts().addAll(savedProducts);

        productRepository.saveAll(order.getProducts());
        Order savedOrder = orderRepository.save(order);

        URI uriLocation = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedOrder.getId())
                .toUri();

        return ResponseEntity.created(uriLocation).build();
    }

    private boolean isProductExists(Product product) {
        return productRepository.existsByNameAndPrice(product.getName(), product.getPrice());
    }

    @GetMapping("/")
    public String rootMapping() {
        return "<h1> Welcome to Sona Super Shop.\nUse \"/list-products\" to see list of available products.";
    }
}
