package com.sarthi.sonasupershop.restcontrollers;

import com.sarthi.sonasupershop.entities.Order;
import com.sarthi.sonasupershop.entities.Product;
import com.sarthi.sonasupershop.repositories.OrderRepository;
import com.sarthi.sonasupershop.repositories.ProductRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    public OrderController(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping("/list-orders")
    public List<Order> retrieveAllOrders() {
        if (orderRepository.findAll().isEmpty())
            throw new RuntimeException("No orders found.");

        return orderRepository.findAll();
    }

    @GetMapping("/orders/{id}")
    public Order retrieveOneOrder(@PathVariable int id) {
        if (!orderRepository.existsById(id))
            throw new RuntimeException("Order #" + id + " does not exists.");

        return orderRepository.findById(id).orElse(null);
    }

    @GetMapping("/orders/{id}/products")
    public List<Product> retrieveAllProductsOfOneOrder(@PathVariable int id) {
        Order order = retrieveOneOrder(id);
        if (order.getProducts() == null || order.getProducts().isEmpty())
            throw new RuntimeException("No products found for Order #" + id);

        return order.getProducts();
    }

    @GetMapping("/orders/{order_id}/products/{product_id}")
    public Product retrieveOneProductOfAnOrder(@PathVariable int order_id, @PathVariable int product_id) {
        retrieveAllProductsOfOneOrder(order_id);

        if (!productRepository.existsById(product_id))
            throw new RuntimeException("Product #" + product_id + " does not exists.");

        return productRepository.findById(product_id).orElse(null);
    }

    //  "/orders/{id}       deleteOrderOfCustomer     DELETE
}
