package com.sarthi.sonasupershop.restcontrollers;

import com.sarthi.sonasupershop.entities.Product;
import com.sarthi.sonasupershop.repositories.ProductRepository;
import jakarta.validation.Valid;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/list-products")
    public List<Product> retrieveAllProducts() {
        if (productRepository.findAll().isEmpty())
            throw new RuntimeException("No products found.");

        return productRepository.findAll();
    }

    /*Important concepts in HATEOAS(Hypermedia As The Engine Of Application State):
        -	EntityModel
		-	WebMvcLinkBuilder*/
    @GetMapping("/products/{id}")
    public EntityModel<Product> retrieveOneProduct(@PathVariable int id) {
        if (!productRepository.existsById(id))
            throw new RuntimeException("Product #" + id + " does not exists.");

        EntityModel<Product> entityModel =
                EntityModel.of(Objects.requireNonNull(productRepository.findById(id).orElse(null)));

        //  Specify the MVC Controller method. DO NOT hard-code the URL because URL may change in the future.
        WebMvcLinkBuilder linkAllProducts = linkTo(methodOn(this.getClass()).retrieveAllProducts());

        //  Add this link to EntityModel's object and then return it.
        entityModel.add(linkAllProducts.withRel("all-products"));

        return entityModel;
    }

    @PostMapping("/products")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody Product product) {

        if (productRepository.existsByNameAndPrice(product.getName(), product.getPrice()))
            throw new RuntimeException("Cannot add Product. " + product.getName() + " with price ₹" + product.getPrice() +
                    " already exists. Please provide a different name or different price.");

        Product savedProduct = productRepository.save(product);

        URI uriLocation = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedProduct.getId())
                .toUri();

        return ResponseEntity.created(uriLocation).build();
    }

    @DeleteMapping("/products/{id}")
    public void removeProduct(@PathVariable int id) {

        if (!productRepository.existsById(id))
            throw new RuntimeException("Cannot remove product because Product #" + id + " does not exists.");

        productRepository.deleteById(id);
    }

    //  Create a POST Mapping to Add Multiple products
}
