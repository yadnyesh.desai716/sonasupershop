package com.sarthi.sonasupershop.entities;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class SqlArrayToListConverter implements AttributeConverter<List<Product>, Product[]> {

    @Override
    public Product[] convertToDatabaseColumn(List<Product> products) {
        if (products == null)
            return new Product[0];
        return products.toArray(new Product[0]);
    }

    @Override
    public List<Product> convertToEntityAttribute(Product[] dbProductArray) {
        if (dbProductArray == null)
            return new ArrayList<>();
        return new ArrayList<>(Arrays.asList(dbProductArray));
    }
}
