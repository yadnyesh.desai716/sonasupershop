package com.sarthi.sonasupershop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Size;

import java.util.List;
import java.util.Objects;

@Entity
public class Customer {

    @Id
    @GeneratedValue
    private Integer id;

    @Size(min = 2, message = "Customer name should have at least 2 characters.")
    private String name;

    @Digits(integer = 10, message = "Mobile should have exactly 10 digits.", fraction = 0)
    private Long mobile;

    @Size(min = 3, message = "Address should have at least 3 characters.")
    private String address;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<Order> orders;

    public Customer() {
    }

    public Customer(Integer id, String name, Long mobile, String address, List<Order> orders) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.orders = orders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cust_id=" + id +
                ", name='" + name + '\'' +
                ", mobile=" + mobile +
                ", address='" + address + '\'' +
                ", orders=" + orders +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof Customer customer))
            return false;

        return Objects.equals(id, customer.id) &&
                Objects.equals(name, customer.name) &&
                Objects.equals(mobile, customer.mobile) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(orders, customer.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, mobile, address, orders);
    }
}
