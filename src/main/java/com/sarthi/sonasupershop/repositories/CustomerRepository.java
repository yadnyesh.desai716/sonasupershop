package com.sarthi.sonasupershop.repositories;

import com.sarthi.sonasupershop.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer>/*, PagingAndSortingRepository<Customer, Integer>*/ {

    boolean existsByNameAndMobile(String name, Long mobile);
}
