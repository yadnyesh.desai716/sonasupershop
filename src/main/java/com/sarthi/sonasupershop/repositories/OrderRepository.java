package com.sarthi.sonasupershop.repositories;

import com.sarthi.sonasupershop.entities.Order;
import com.sarthi.sonasupershop.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllOrdersByCustomerId(int id);

    Order findOrderByCustomerIdAndId(int cust_id, int order_id);

    List<Product> findAllProductsById(int order_id);
}
