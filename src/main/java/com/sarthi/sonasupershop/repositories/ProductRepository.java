package com.sarthi.sonasupershop.repositories;

import com.sarthi.sonasupershop.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

//    void saveAllIfNotExists(List<Product> products);

//    void saveIfNotExistsByNameAndPrice(String name, double price);

    boolean existsByNameAndPrice(String name, double price);

    Product findByNameAndPrice(String name, double price);
}
