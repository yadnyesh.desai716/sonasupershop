package com.sarthi.sonasupershop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonaSuperShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonaSuperShopApplication.class, args);
	}

}
